variable "cloudflare_account_name" {
  type        = string
  description = "CloudFlare account name"
  default     = "mwwaaa"
}

variable "cloudflare_api_token" {
  type        = string
  sensitive   = true
  description = "Cloudflare API token"
}

variable "kubernetes_context" {
  type        = string
  default     = "minikube"
  description = "Homedir kubeconfig kubernetes context"
}

variable "domain" {
  type    = string
  default = "walzberg.fr"
}

variable "prefix" {
  type        = string
  description = "Prefix applied to names"
  default     = ""
}

variable "suffix" {
  type        = string
  description = "Suffix applied to names"
  default     = ""
}
