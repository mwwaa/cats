resource "random_bytes" "tunnel" {
  length = 64
}

locals {
  fqdn                  = "${local.name}.${var.domain}"
}

resource "cloudflare_tunnel" "ollama" {
  account_id = local.cloudflare_account_id
  secret     = random_bytes.tunnel.base64
  name       = local.name
}

resource "cloudflare_tunnel_config" "ollama" {
  account_id = local.cloudflare_account_id
  tunnel_id  = cloudflare_tunnel.ollama.id
  config {
    ingress_rule {
      service  = "http://${kubernetes_service.ollama.metadata[0].name}:${local.lama_port}"
      hostname = local.fqdn
      path     = "/api/generate"
    }
    ingress_rule {
      service = "http_status:404"
    }
  }
}

resource "cloudflare_access_service_token" "token" {
  account_id = local.cloudflare_account_id
  name       = local.name
}

resource "cloudflare_access_application" "ollama" {
  name                      = local.name
  zone_id                   = local.cloudflare_zone_id
  app_launcher_visible      = false
  auto_redirect_to_identity = true
  domain                    = local.fqdn
}

resource "cloudflare_access_policy" "ollama" {
  zone_id        = local.cloudflare_zone_id
  application_id = cloudflare_access_application.ollama.id
  precedence     = 1
  name           = "Service token"
  decision       = "non_identity"
  include {
    service_token = [cloudflare_access_service_token.token.id]
  }
  require {
    service_token = [cloudflare_access_service_token.token.id]
  }
}

resource "cloudflare_record" "ollama" {
  name    = local.name
  type    = "CNAME"
  zone_id = local.cloudflare_zone_id
  value   = cloudflare_tunnel.ollama.cname
  proxied = true
}
