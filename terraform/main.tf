data "cloudflare_accounts" "account" {
  name = var.cloudflare_account_name
}

data "cloudflare_zone" "domain" {
  name = var.domain
}

locals {
  name                  = "${var.prefix}cats${var.suffix}"
  lama_port             = 11434
  cloudflare_account_id = data.cloudflare_accounts.account.accounts[0].id
  cloudflare_zone_id    = data.cloudflare_zone.domain.zone_id
}
