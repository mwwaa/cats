provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = var.kubernetes_context
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

provider "random" {}
