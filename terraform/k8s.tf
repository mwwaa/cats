locals {
  lama_selector = "ollama"
  ns            = kubernetes_namespace.cats.metadata[0].name
}

resource "kubernetes_namespace" "cats" {
  metadata {
    name = local.name
  }
}

resource "kubernetes_deployment" "ollama" {
  metadata {
    name      = "ollama"
    namespace = local.ns
  }
  spec {
    selector {
      match_labels = {
        ingress = local.lama_selector
      }
    }
    replicas = 1
    template {
      metadata {
        name = "ollama"
        labels = {
          ingress = local.lama_selector
        }
      }
      spec {
        container {
          name  = "ollama"
          image = "ollama/ollama:0.1.13"
          port {
            container_port = local.lama_port
          }
          readiness_probe {
            http_get {
              port = local.lama_port
              path = "/"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "ollama" {
  metadata {
    name      = local.name
    namespace = local.ns
  }
  spec {
    selector = {
      ingress = local.lama_selector
    }
    type = "ClusterIP"
    port {
      port        = local.lama_port
      target_port = local.lama_port
    }
  }
}

resource "kubernetes_secret" "tunnel" {
  metadata {
    name      = "tunnel"
    namespace = local.ns
  }
  data = {
    token = cloudflare_tunnel.ollama.tunnel_token
  }
}

resource "kubernetes_deployment" "tunnel" {
  metadata {
    name      = "cloudflared"
    namespace = local.ns
  }
  spec {
    replicas = 2
    selector {
      match_labels = {
        tunnel = "ollama"
      }
    }
    template {
      metadata {
        name = "cloudflared"
        labels = {
          tunnel = "ollama"
        }
      }
      spec {
        container {
          name  = "cloudflared"
          image = "cloudflare/cloudflared:latest"
          args = [
            "tunnel",
            "run",
            # "--metrics",
            # "0.0.0.0:2000",
          ]
          env {
            name = "TUNNEL_TOKEN"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.tunnel.metadata[0].name
                key  = "token"
              }
            }
          }
          # liveness_probe {
          #   http_get {
          #     path = "/ready"
          #     port = 2000
          #   }
          # }
        }
      }
    }
  }
}
